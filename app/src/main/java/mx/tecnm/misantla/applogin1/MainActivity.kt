package mx.tecnm.misantla.applogin1

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.IdpResponse
import com.google.firebase.auth.FirebaseAuth
import mx.tecnm.misantla.applogin1.databinding.ActivityMainBinding
import java.util.*

class MainActivity : AppCompatActivity() {
    // Variables FirebaseUI
    private lateinit var binding: ActivityMainBinding
    private val MY_REQUEST_CODE : Int = 1234
    lateinit var  providers: List<AuthUI.IdpConfig>

    // Variables RecyclerView
    lateinit var irAmerica : Button
    var bandera = ""
    var lista2 : RecyclerView? = null
    var adaptador : adaptadorCustom? = null
    var layoutManager : RecyclerView.LayoutManager? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)

        setContentView(binding.root)

        providers = Arrays.asList<AuthUI.IdpConfig>(
            AuthUI.IdpConfig.GoogleBuilder().build(),
            AuthUI.IdpConfig.FacebookBuilder().build()
        )

        TiposdeLogin()

        binding.btnCerrar.setOnClickListener {
            AuthUI.getInstance().signOut(this)
                .addOnCompleteListener {
                    binding.btnCerrar.isEnabled = false
                    TiposdeLogin()
                }
                .addOnFailureListener {
                        e -> Toast.makeText(this, e.message, Toast.LENGTH_LONG).show()
                }
        }

        // RecyclerView
        val continentes = ArrayList<menuC>()

        irAmerica = findViewById(R.id.irAmerica) as Button
        irAmerica.setOnClickListener {
            if (bandera == "America") {
                val intento1 = Intent(this, America::class.java)
                startActivity(intento1)
            }
            else if (bandera == "Europa") {
                val intento1 = Intent(this, Europa::class.java)
                startActivity(intento1)
            }
            else if (bandera == "Asia") {
                val intento1 = Intent(this, Asia::class.java)
                startActivity(intento1)
            }
            else if (bandera == "Oceania") {
                val intento1 = Intent(this, Oceania::class.java)
                startActivity(intento1)
            }
            else if (bandera == "Africa") {
                val intento1 = Intent(this, Africa::class.java)
                startActivity(intento1)
            }
            else {
                Toast.makeText(applicationContext, "Seleccione un continentne", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }
        }
        continentes.add(menuC("America", 1002, R.drawable.america))
        continentes.add(menuC("Europa", 7414, R.drawable.europa))
        continentes.add(menuC("Asia", 4463, R.drawable.asia))
        continentes.add(menuC("Oceania", 4174, R.drawable.australia))
        continentes.add(menuC("Africa", 1216, R.drawable.africa))

        lista2 = findViewById(R.id.Lista2)
        lista2?.setHasFixedSize(true)
        layoutManager = LinearLayoutManager(this)
        lista2?.layoutManager = layoutManager
        adaptador = adaptadorCustom(this, continentes, object : ClickListener {
            override fun onClick(vista: View, index: Int) {
                Toast.makeText(applicationContext, continentes.get(index).nombre, Toast.LENGTH_LONG).show()
                bandera = continentes.get(index).nombre
            }
        })
        lista2?.adapter =adaptador
    }

    private fun TiposdeLogin() {
        startActivityForResult(AuthUI.getInstance().createSignInIntentBuilder()
            .setAvailableProviders(providers)
            .build(),MY_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == MY_REQUEST_CODE){
            val response = IdpResponse.fromResultIntent(data)
            if(resultCode == Activity.RESULT_OK){
                var user = FirebaseAuth.getInstance().currentUser
                Toast.makeText(this, ""+user!!.email, Toast.LENGTH_LONG).show()
                binding.btnCerrar.isEnabled = true
            }else{
                Toast.makeText(this, "Error: "+response!!.error!!.message, Toast.LENGTH_LONG).show()
            }
        }

    }

}