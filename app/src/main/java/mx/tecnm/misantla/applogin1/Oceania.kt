package mx.tecnm.misantla.applogin1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class Oceania : AppCompatActivity() {

    var listaO: RecyclerView? = null
    var adaptador:adaptadorCustomOceania? = null
    var layoutManager: RecyclerView.LayoutManager? = null   //el diseño

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_oceania)
        val oceaniaA = ArrayList<menuOceania>()

        oceaniaA.add(menuOceania("Melbourne", R.drawable.melbourne))
        oceaniaA.add(menuOceania("Brisbane",  R.drawable.brisbane))
        oceaniaA.add(menuOceania("Perth",  R.drawable.perth))
        oceaniaA.add(menuOceania("Gold Coast",  R.drawable.gold_coast))
        oceaniaA.add(menuOceania("Adelaida",  R.drawable.adelaida))


        listaO = findViewById(R.id.Lista5)
        listaO?.setHasFixedSize(true)  //adaptador tamaño de la vista

        layoutManager = LinearLayoutManager(this)
        listaO?.layoutManager = layoutManager  // donde se dibuje el layout

        adaptador = adaptadorCustomOceania(this, oceaniaA, object : ClickListener {
            override fun onClick(vista: View, index: Int) {
                Toast.makeText(applicationContext, oceaniaA.get(index).nombreA, Toast.LENGTH_LONG).show()

            }

        })

        listaO?.adapter = adaptador


    }


}
