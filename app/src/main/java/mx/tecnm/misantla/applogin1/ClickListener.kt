package mx.tecnm.misantla.applogin1

import android.view.View

interface ClickListener {
    fun onClick(vista: View, index:Int)
}
