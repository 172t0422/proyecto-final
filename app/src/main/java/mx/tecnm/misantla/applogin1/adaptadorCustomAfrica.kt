package mx.tecnm.misantla.applogin1

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class adaptadorCustomAfrica(var context: Context, items: ArrayList<menuAfrica>, var listener: ClickListener): RecyclerView.Adapter<adaptadorCustomAfrica.ViewHolder>() {
    var items: ArrayList<menuAfrica>? = null
    init {
        this.items = items
    }
    // el archivo xml en viewHolder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): adaptadorCustomAfrica.ViewHolder {
        val vista = LayoutInflater.from(context).inflate(R.layout.menuafrica,parent,false)
        val viewHolder = ViewHolder(vista,listener)
        return viewHolder
    }
    override fun onBindViewHolder(holder: adaptadorCustomAfrica.ViewHolder, position: Int) {
        val item = items?.get(position)
        holder.foto?.setImageResource(item?.fotoA!!)
        holder.nombre?.text = item?.nombreA
    }
    override fun getItemCount(): Int {
        return items?.count()!!
    }
    class ViewHolder(vista: View, listener: ClickListener): RecyclerView.ViewHolder(vista), View.OnClickListener{
        var vista = vista
        var foto: ImageView? = null
        var nombre: TextView? = null
        var listener:ClickListener? = null
        init {
            foto = vista.findViewById(R.id.imView)
            nombre = vista.findViewById(R.id.tvNombre)
            this.listener = listener
            vista.setOnClickListener(this)
        }
        override fun onClick(v: View?) {
            this.listener?.onClick(v!!,adapterPosition)
        }
    }
}