package mx.tecnm.misantla.applogin1
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
class Asia : AppCompatActivity() {
    var listaA: RecyclerView? = null
    var adaptador:adaptadorCustomAsia? = null
    var layoutManager: RecyclerView.LayoutManager? = null   //el diseño
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_asia)
        val asiaA = ArrayList<menuAsia>()
        asiaA.add(menuAsia("Hong kong", R.drawable.hong_kong))
        asiaA.add(menuAsia("Bangkok",  R.drawable.bangkok))
        asiaA.add(menuAsia("Londres",  R.drawable.londres))
        asiaA.add(menuAsia("Singapur",  R.drawable.singapur))
        asiaA.add(menuAsia("Macao",  R.drawable.macao))
        listaA = findViewById(R.id.Lista4)
        listaA?.setHasFixedSize(true)  //adaptador tamaño de la vista
        layoutManager = LinearLayoutManager(this)
        listaA?.layoutManager = layoutManager  // donde se dibuje el layout
        adaptador = adaptadorCustomAsia(this, asiaA, object : ClickListener {
            override fun onClick(vista: View, index: Int) {
                Toast.makeText(applicationContext, asiaA.get(index).nombreA, Toast.LENGTH_LONG).show()
            }
        })
        listaA?.adapter = adaptador
    }
}