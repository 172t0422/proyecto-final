package mx.tecnm.misantla.applogin1

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlin.collections.ArrayList
class adaptadorCustomAmerica(var context: Context, items: ArrayList<menuAmerica>, var listener: ClickListener): RecyclerView.Adapter<adaptadorCustomAmerica.ViewHolder>() {
    var items: ArrayList<menuAmerica>? = null
    init {
        this.items = items
    }
    // el archivo xml en viewHolder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): adaptadorCustomAmerica.ViewHolder {
        val vista = LayoutInflater.from(context).inflate(R.layout.menuamerica,parent,false)
        val viewHolder = ViewHolder(vista,listener)
        return viewHolder
    }
    override fun onBindViewHolder(holder: adaptadorCustomAmerica.ViewHolder, position: Int) {
        val item = items?.get(position)
        holder.foto?.setImageResource(item?.fotoA!!)
        holder.nombre?.text = item?.nombreA
    }
    override fun getItemCount(): Int {
        return items?.count()!!
    }
    class ViewHolder(vista: View, listener: ClickListener): RecyclerView.ViewHolder(vista), View.OnClickListener{
        var vista = vista
        var foto: ImageView? = null
        var nombre: TextView? = null
        var listener:ClickListener? = null
        init {
            foto = vista.findViewById(R.id.imView)
            nombre = vista.findViewById(R.id.tvNombre)
            this.listener = listener
            vista.setOnClickListener(this)
        }
        override fun onClick(v: View?) {
            this.listener?.onClick(v!!,adapterPosition)
        }
    }
}