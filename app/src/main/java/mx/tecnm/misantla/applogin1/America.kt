package mx.tecnm.misantla.applogin1
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class America : AppCompatActivity() {
    var listaA: RecyclerView? = null
    var adaptador:adaptadorCustomAmerica? = null
    var layoutManager: RecyclerView.LayoutManager? = null   //el diseño
        override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_america)
        val americaA = ArrayList<menuAmerica>()
        americaA.add(menuAmerica("Cancun", R.drawable.cancun))
        americaA.add(menuAmerica("Santiago de Chile",  R.drawable.chile))
        americaA.add(menuAmerica("Brazil",  R.drawable.brazil))
        americaA.add(menuAmerica("Buenos Aires",  R.drawable.buenos_aires))
        americaA.add(menuAmerica("Panama",  R.drawable.panama))
        listaA = findViewById(R.id.Lista3)
        listaA?.setHasFixedSize(true)  //adaptador tamaño de la vista
        layoutManager = LinearLayoutManager(this)
        listaA?.layoutManager = layoutManager  // donde se dibuje el layout
        adaptador = adaptadorCustomAmerica(this, americaA, object : ClickListener {
            override fun onClick(vista: View, index: Int) {
                Toast.makeText(applicationContext, americaA.get(index).nombreA, Toast.LENGTH_LONG).show()
            }
        })
        listaA?.adapter = adaptador
    }
}
