package mx.tecnm.misantla.applogin1

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlin.collections.ArrayList

class adaptadorCustom(var context: Context, items: ArrayList<menuC>, var listener: ClickListener): RecyclerView.Adapter<adaptadorCustom.ViewHolder>() {

    var items:ArrayList<menuC>? = null
    init {
        this.items = items
    }
    // el archivo xml en viewHolder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): adaptadorCustom.ViewHolder {
        val vista = LayoutInflater.from(context).inflate(R.layout.menu,parent,false)
        val viewHolder = ViewHolder(vista,listener)
        return viewHolder
    }
    override fun onBindViewHolder(holder: adaptadorCustom.ViewHolder, position: Int) {
        val item = items?.get(position)
        holder.foto?.setImageResource(item?.foto!!)
        holder.nombre?.text = item?.nombre
        holder.poblacion?.text = "poblacion: " + item?.poblacion.toString()+"x10^9"
    }
    override fun getItemCount(): Int {
        return items?.count()!!
    }
    class ViewHolder(vista: View, listener: ClickListener): RecyclerView.ViewHolder(vista), View.OnClickListener{
        var vista = vista
        var foto: ImageView? = null
        var nombre: TextView? = null
        var poblacion: TextView? = null
        var listener:ClickListener? = null
        init {
            foto = vista.findViewById(R.id.imView)
            nombre = vista.findViewById(R.id.tvNombre)
            poblacion = vista.findViewById(R.id.tvPoblacion)
            this.listener = listener
            vista.setOnClickListener(this)
        }
        override fun onClick(v: View?) {
            this.listener?.onClick(v!!,adapterPosition)
        }
    }
}