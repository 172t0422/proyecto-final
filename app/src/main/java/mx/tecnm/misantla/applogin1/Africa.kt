package mx.tecnm.misantla.applogin1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
class Africa : AppCompatActivity() {
    var listaAA: RecyclerView? = null
    var adaptador:adaptadorCustomAfrica? = null
    var layoutManager: RecyclerView.LayoutManager? = null   //el diseño
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_africa)
        val africaA = ArrayList<menuAfrica>()
        africaA.add(menuAfrica("Tunez", R.drawable.tunez))
        africaA.add(menuAfrica("Argelia",  R.drawable.argelia))
        africaA.add(menuAfrica("Mozanbique",  R.drawable.mozambique))
        africaA.add(menuAfrica("Kenia",  R.drawable.kenia))
        africaA.add(menuAfrica("Tanzania",  R.drawable.tanzania))
        listaAA = findViewById(R.id.Lista6)
        listaAA?.setHasFixedSize(true)  //adaptador tamaño de la vista
        layoutManager = LinearLayoutManager(this)
        listaAA?.layoutManager = layoutManager  // donde se dibuje el layout
        adaptador = adaptadorCustomAfrica(this, africaA, object : ClickListener {
            override fun onClick(vista: View, index: Int) {
                Toast.makeText(applicationContext, africaA.get(index).nombreA, Toast.LENGTH_LONG).show()
            }
        })
        listaAA?.adapter = adaptador
    }
}
