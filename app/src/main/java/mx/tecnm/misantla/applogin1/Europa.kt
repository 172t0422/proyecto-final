package mx.tecnm.misantla.applogin1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class Europa : AppCompatActivity() {
    var listaE: RecyclerView? = null
    var adaptador:adaptadorCustomEuropa? = null
    var layoutManager: RecyclerView.LayoutManager? = null   //el diseño

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_europa)

        val europaA = ArrayList<menuEuropa>()

        europaA.add(menuEuropa("Estambul", R.drawable.estambul))
        europaA.add(menuEuropa("Roma",  R.drawable.roma))
        europaA.add(menuEuropa("Paris",  R.drawable.paris))
        europaA.add(menuEuropa("Barcelona",  R.drawable.barcelona))
        europaA.add(menuEuropa("Praga",  R.drawable.praga))


        listaE = findViewById(R.id.Lista4)
        listaE?.setHasFixedSize(true)  //adaptador tamaño de la vista

        layoutManager = LinearLayoutManager(this)
        listaE?.layoutManager = layoutManager  // donde se dibuje el layout

        adaptador = adaptadorCustomEuropa(this, europaA, object : ClickListener {
            override fun onClick(vista: View, index: Int) {
                Toast.makeText(applicationContext, europaA.get(index).nombreA, Toast.LENGTH_LONG).show()

            }

        })

        listaE?.adapter = adaptador


    }


}